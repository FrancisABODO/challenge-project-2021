import React from "react";
import { Link } from "react-router-dom";

export default function CardItem() {
  return (
    <>
      <li className="cards__item">
        <Link className="cards__item__link">
          <figure className="cards__item__pic-wrap">
            <img src="" alt="" className="cards__item__img" />
          </figure>
        </Link>
      </li>
    </>
  );
}
